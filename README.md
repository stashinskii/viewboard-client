## Viewboard Project: React SPA

React.js based client application for Viewboard API to automize data processing and aggregate this data within Machine Learning. More details at [Viewboard API Page](https://bitbucket.org/stashinskii/viewboard/src)  


In the project directory, you can run `npm start` to build and runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You may also run this client app through Visual Studio (run script configured in ASP.NET Core) under [https://localhost:44353/](https://localhost:44353/) to view it in the browser.
