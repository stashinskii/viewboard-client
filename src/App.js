import React, { Component } from 'react';

import AppHeader from './components/AppHeader/AppHeader';
import AppFooter from './components/AppFooter/AppFooter';
import AppMain from './components/AppMain/AppMain';
import { history } from './helpers';
import { alertActions } from './actions';
import { PrivateRoute } from './components/PrivateRoute';
import { LoginPageContainer } from './components/LoginPage/LoginPageContainer';
import { SignUpPage } from './components/SignUpPage/SignUpPage';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';

class App extends Component {

  constructor(props) {
    super(props);

    history.listen((location, action) => {
        // clear alert on location change
        this.props.clearAlerts();
    });
  }

  render() {
    console.log(this.props)
    return (
      <div>
        {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
        <AppHeader />
        
        <Router history={history}>
                            <div>
                                <PrivateRoute exact path="/" component={AppMain} />
                                <Route path="/login" component={LoginPageContainer} />
                                <Route path="/register" component={SignUpPage} />
                            </div>
                        </Router>
     
      </div>
    );
  } 
}    

function mapState(state) {
  console.log("Вызвали mapState в App.js")
  const { alert, authentication } = state;
  return { alert, authentication };
}

const actionCreators = {
  clearAlerts: alertActions.clear
};

export default connect(mapState, actionCreators)(App);