import LoginPage from "./LoginPage"
import React from "react"
import { userActions } from '../../actions';
import { connect } from 'react-redux';


class LoginPageContainer extends React.Component{
    constructor(props){
      super(props)
      this.props.logout();
    }
  
    render(){
      return(
        <LoginPage login={this.props.login}/>
      )
    }
  }
  
  function mapState(state) {
    const { authentication } = state;
    return { authentication };
}

const actionCreators = {
    login: userActions.login,
    logout: userActions.logout
};
  
const loginPage = connect(mapState, actionCreators)(LoginPageContainer);
export { loginPage as LoginPageContainer };