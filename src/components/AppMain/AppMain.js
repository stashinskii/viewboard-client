import React, { Component } from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import SimplePostCard from '../Card/SimplePostCard'
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
      },
      gridList: {
        flexWrap: 'nowrap',
        
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
      },
      title: {
        color: theme.palette.primary.light,
      },
      titleBar: {
        background:
          'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
      },
      container: {
          marginTop: '10px'
      },
    }));

const ListWrapper = (props) => {
      const classes = useStyles();

      const getGridListCols = (props) => {
        console.log("GRIIID")
        console.log(props)
        if (isWidthUp('xl', props.width)) {
          return 4;
        }
    
        if (isWidthUp('lg', props.width)) {
          return 3;
        }
    
        if (isWidthUp('md', props.width)) {
          return 2;
        }
    
        return 1;
      }

      return (
        <Container className={classes.container}>
        <h4>Top rated places</h4>
        <div className={classes.root}>
       
        <GridList cellHeight='auto' className={classes.gridList} cols={ getGridListCols(props) }>
            <GridListTile key='1'>
              <SimplePostCard title="The 10 best beach holidays in Turkey for 2019" description="Our expert pick of the best beach holidays in Turkey for 2019. Follow this link for our guide to the country’s best hotels." url="https://www.telegraph.co.uk/travel/destinations/europe/turkey/articles/The-top-10-beach-holidays-in-Turkey/" image="https://www.telegraph.co.uk/content/dam/Travel/2019/May/turkey-beach-dalyan.jpg?imwidth=1400"/>
            </GridListTile> 
            <GridListTile key='2' >
              <SimplePostCard title="Villages" description="A village is a part of a world clustered human settlement or community, larger than a hamlet but smaller than a town, with a population ranging from a few hundred to a few thousand." image="https://images.pexels.com/photos/414612/pexels-photo-414612.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"/>
            </GridListTile>    
            <GridListTile  key='3' >
              <SimplePostCard title="Nature" description="N/A" image="https://images.pexels.com/photos/814499/pexels-photo-814499.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"/>
            </GridListTile>  
            <GridListTile  key='4' >
              <SimplePostCard title="Country" description="2,976 free country pictures" image="https://images.unsplash.com/photo-1500382017468-9049fed747ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"/>
            </GridListTile>  
            <GridListTile key='5'>
              <SimplePostCard title="The 10 best beach holidays in Turkey for 2019" description="Our expert pick of the best beach holidays in Turkey for 2019. Follow this link for our guide to the country’s best hotels." url="https://www.telegraph.co.uk/travel/destinations/europe/turkey/articles/The-top-10-beach-holidays-in-Turkey/" image="https://www.telegraph.co.uk/content/dam/Travel/2019/May/turkey-beach-dalyan.jpg?imwidth=1400"/>
            </GridListTile>    
        </GridList>
      </div>
      </Container>
      )
}

class AppMain extends Component { 
  render() {
    return (
        <main>
            <ListWrapper width={this.props.width}/>
        </main>
    );
  }
}

export default withWidth()(AppMain);